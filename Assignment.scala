case class Point(a: Int, b:Int){

	def x:Int=a
	def y:Int=b

	def move(dx:Int, dy:Int)=Point(this.x+dx, this.y+dy)
	def+(that:Point)=Point(this.x+that.x, this.y+that.y)
	def-(that:Point)=Point(this.x-that.x, this.y-that.y)
	def DisOfpoints(that:Point):Double=
	{

		val distance=math.sqrt((this.x)*(this.x)+(this.y)*(this.y))
		return distance
	}

	def InvertOfpoint(that:Point)=Point(this.y,this.x)
}

object Assignment{

	def main(args:Array[String]){

		val p1=Point(5,3)
		val p2=Point(3,2)
		val p3=p1.move(1,2)
		val p4=p1-p2
		
		println(p1)
		println(p2)

		print("Method of move:")
		println(p3)
		
		print("Method of add:")
		println(p1+p2)

		print("Method of distance:")
		println(p4.DisOfpoints(p4))

		print("Method of invert:")
		println(p1.InvertOfpoint(p1))
	}
}